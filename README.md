note this is just a fork of https://github.com/dreamsofcode-io/neovim-python

# How to use

To install this for your neovim configuration

```
$ git clone https://gitlab.com/GenkiTheMuffin/neovim-python.git  ~/.config/nvim/lua/custom
```

Then open up neovim and let everything install.

Restart Neovim and install the treesitter syntax

```
:TSInstall python
```
